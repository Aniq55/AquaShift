#!/usr/bin/env python

import subprocess
import ast
from flask import Flask, jsonify, g, redirect, request, url_for
import os

app = Flask(__name__)

@app.teardown_request
def teardown_request(exception):
    pass

@app.route('/')
def index():
    return redirect(url_for('static', filename='x.html'))

@app.route('/json-data/')
def json_data():
    # get number of items from the javascript request
    coordinates = request.args.get('input')
    coordinates = '\n'.join(coordinates.split(',')) + '\n'
    print("Input:", coordinates)
    output = subprocess.check_output(['python3', os.path.dirname(os.path.realpath(__file__)) + '/overlap.py'],
                                     input=coordinates,
                                     universal_newlines=True)
    print("Output:", output)
    if (output != "Error\n"):
        output = ast.literal_eval('[' + ', '.join(output.split('\n')) + ']')
    else:
        output = output.split("\n")[0]
    return jsonify(dict({"output": output}))

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001) # http://localhost:5001/
else:
    application = app # for a WSGI server e.g.,
    # twistd -n web --wsgi=hello_world.application --port tcp:5001:interface=localhost
