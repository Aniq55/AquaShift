import pandas as pd
# from osgeo import gdal
import numpy as np


FILTER = 7

cell_size = 0.04166666667
xll = -180.0
yll = -58.015072


def toLat(x):
    return xll + x*cell_size

def toLon(y):
    return yll + y*cell_size

xll2 = -180.0
yll2 = -57.999999999974

def toLat2(x):
    return xll2 + x*cell_size

def toLon2(y):
    return yll2 + y*cell_size

df = pd.read_table("gdfld/gdfld.asc")
df = df.drop(df.index[0:5]) # dropping the data details

valid_datapoints=[]
flood_val=[]

for i, row in enumerate(df.values):
    x= (str(row)[2:-3]).split()
    for j in range(len(x)):
        if x[j]!='-9999':
            if int(x[j])>FILTER:
                valid_datapoints.append((i,j))
                flood_val.append(int(x[j]))

# print(len(valid_datapoints))
# print(len(flood_val))

valid_pos=[]

for p in valid_datapoints:
    valid_pos.append((toLon(p[0]), toLat(p[1])))



df2 = pd.read_table("gddrg/gddrg.asc")
df2 = df2.drop(df2.index[0:5]) # dropping the data details


valid_datapoints2=[]
drought_val=[]

for i, row in enumerate(df2.values):
    x= (str(row)[2:-3]).split()
    for j in range(len(x)):
        if x[j]!='-9999':
            if int(x[j])>FILTER:
                valid_datapoints2.append((i,j))
                drought_val.append(int(x[j]))

# print(len(valid_datapoints))
# print(len(flood_val))

valid_pos2=[]

for p in valid_datapoints2:
    valid_pos2.append((toLon2(p[0]), toLat2(p[1])))

import matplotlib.pyplot as plt


print(len(flood_val))
print(len(drought_val))

j=0
for x in valid_pos[0:10000]:
    plt.scatter(x[1],x[0], color='b',s= flood_val[j])
    j=j+1

j=0
for x in valid_pos2[0:10000]:
    plt.scatter(x[1],x[0], color='r', s= drought_val[j])
    j=j+1

plt.show()
plt.savefig('fld_drg.png')

# print(valid_pos)





# print(df)
