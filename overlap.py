import pandas as pd
import numpy as np
import os

# User Inputs
FILTER = 5

mid_x = float(input())
mid_y = float(input())

def fix_x(n):
    if (n < -180):
        return -180
    if (n > 180):
        return 180
    return n

def fix_y(n):
    if (n < -90):
        return -90
    if (n > 90):
        return 90
    return n

TL_x = fix_x(mid_x - 4)   # 86
TL_y = fix_y(mid_y + 4)   # 26
BR_x = fix_x(mid_x + 4)   # 89
BR_y = fix_y(mid_y - 4)   # 21


cell_size = 0.0416666666667

# validate -58<x<85 and -180<y<-180
j_start = int((TL_x+180.0)/cell_size)
j_end = int((BR_x+180.0)/cell_size)
i_start = int((-TL_y+85.0)/cell_size)
i_end = int((-BR_y+85.0)/cell_size)


xll = -180.0
yll = -58.015072


def toLat(x):
    return xll + (x+0.5)*cell_size

def toLon(y):
    return 143 + yll - (y+0.5)*cell_size

xll2 = -180.0
yll2 = -57.999999999974

def toLat2(x):
    return xll2 + (x+0.5)*cell_size

def toLon2(y):
    return 143 + yll2 - (y+0.5)*cell_size

df = pd.read_table(os.path.dirname(os.path.realpath(__file__)) + "/gdfld/gdfld.asc")
df = df.drop(df.index[0:5]) # dropping the data details

valid_datapoints=[]
flood_val=[]

for i, row in enumerate(df.values):
    if i> i_start and i< i_end:
        x= (str(row)[2:-3]).split()
        for j in range(j_start, j_end+1):
            if x[j]!='-9999':
                if int(x[j])>FILTER:
                    valid_datapoints.append((i,j))
                    flood_val.append(int(x[j]))


valid_pos=[]

for p in valid_datapoints:
    valid_pos.append((toLon(p[0]), toLat(p[1])))



df2 = pd.read_table(os.path.dirname(os.path.realpath(__file__)) + "/gddrg/gddrg.asc")
df2 = df2.drop(df2.index[0:5]) # dropping the data details


valid_datapoints2=[]
drought_val=[]

for i, row in enumerate(df2.values):
    if i> i_start and i< i_end:
        x= (str(row)[2:-3]).split()
        for j in range(j_start, j_end+1):
            if x[j]!='-9999':
                if int(x[j])>FILTER:
                    valid_datapoints2.append((i,j))
                    drought_val.append(int(x[j]))

valid_pos2=[]


for p in valid_datapoints2:
    valid_pos2.append((toLon2(p[0]), toLat2(p[1])))

from sklearn.cluster import KMeans

kmeans = KMeans(n_clusters=4)

if (len(valid_pos) == 0 or len(valid_pos2) == 0):
    print("Error")
    exit(0)
else:
    print([[fix_x(mid_x - 4), fix_y(mid_y + 4)], [fix_x(mid_x + 4), fix_y(mid_y + 4)]])
    print([[fix_x(mid_x + 4), fix_y(mid_y + 4)], [fix_x(mid_x + 4), fix_y(mid_y - 4)]])
    print([[fix_x(mid_x + 4), fix_y(mid_y - 4)], [fix_x(mid_x - 4), fix_y(mid_y - 4)]])
    print([[fix_x(mid_x - 4), fix_y(mid_y - 4)], [fix_x(mid_x - 4), fix_y(mid_y + 4)]])

kmeans_fld = kmeans.fit(valid_pos)
centroids = kmeans_fld.cluster_centers_

kmeans_drg = kmeans.fit(valid_pos2)
centroids2 = kmeans_fld.cluster_centers_


class Pair:
    def __init__(self, d,f):
        self.d= d
        self.f= f
        self.dist= 0.0
    def cal_dist(self):
        phi1= np.deg2rad(self.d[0])
        phi2= np.deg2rad(self.f[0])
        lam1= np.deg2rad(self.d[1])
        lam2= np.deg2rad(self.f[1])
        a= np.square(np.sin((phi1-phi2)/2)) + np.cos(phi1)*np.cos(phi2)*np.square(np.sin((lam1-lam2)/2))
        self.dist= 6371000.0*2*np.arctan2(np.sqrt(a), np.sqrt(1-a))


PAIRS= []

for i in centroids:
    for j in centroids2:
        PAIRS.append(Pair(i,j))

for p in PAIRS:
    p.cal_dist()

def plot_pair(pair):
    if pair.dist < 500000:
        print([[pair.f[1], pair.f[0]], [pair.d[1], pair.d[0]]])

for p in PAIRS:
    plot_pair(p)
