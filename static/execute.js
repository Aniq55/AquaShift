var bodyPadding = document.body.style.paddingTop;
var showMapFlag = false;
window.resize = function (event) {
    if (showMapFlag == false)
        bodyPadding = document.body.style.paddingTop;
}

// For XHR to the flask server
function loadXMLDoc(input, callback) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
            if (xmlhttp.response != "Error" && xmlhttp.response != "") {
                let output = JSON.parse(xmlhttp.response);
                callback(output);
            }
        }
    };
    xmlhttp.open("GET", "/json-data/?input=" + input, true);
    xmlhttp.send();
}

// Shows the map
function showMap() {
    document.getElementById("map").style.display = "block";
    document.getElementById("container").style.display = "none";
    bodyPadding = document.body.style.paddingTop;
    document.body.style.paddingTop = "0px";
    document.getElementsByTagName("h2")[0].style.paddingBottom = "30px";
    showMapFlag = true;
}

// Hide map
function hideMap() {
    let map = document.getElementById("map");
    map.parentNode.removeChild(map);
    let newMap = document.createElement("div");
    newMap.id = "map";
    document.body.appendChild(newMap);
    document.getElementById("container").style.display = "block";
    document.body.style.paddingTop = bodyPadding;
    document.getElementsByTagName("h2")[0].style.paddingBottom = "0px";
    showMapFlag = false;
    document.getElementsByTagName("button")[0].disabled = false;
}

// executes the process on button submit
function execute() {
    // Trim spaces from input
    let input = document.getElementById("input").value.replace(/\s/g, "");
    let isNumber = true;
    let coods = input.split(',');
    coods.forEach(function (item, index) {
        if (Number.isNaN(parseInt(item)))
            isNumber = false;
    });
    if (isNumber && coods.length == 2) {
        document.getElementsByTagName("button")[0].disabled = true;
        loadXMLDoc(input, response => {
            if (response.output == "Error") {
                alert("No flood to drought links are possible.");
                document.getElementsByTagName("button")[0].disabled = false;
                return;
            }
            console.log("Response:", response.output);
            input = input.split(",");
            input = [ parseInt(input[0]),
                      parseInt(input[1]) ];
            showMap();
            plot(input, response.output);
        });
    }
    else
        alert("Make sure the input consits of a longitude and a latitude separated by a comma.")
}
