function plot(center, data) {
    let map = new ol.Map({
        target: 'map',
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat(center),
            zoom: 6
        })
    });

    let drgPoints = [];
    let fldPoints = [];

    data.forEach(function (points, index) {

        for (let i = 0; i < points.length; i++) {
            points[i] = ol.proj.transform(points[i], 'EPSG:4326', 'EPSG:3857');
        }

        if (index > 3) {
            let fldPoint = new ol.Feature({
                geometry: new ol.geom.Point(points[0])
            });
            let drgPoint = new ol.Feature({
                geometry: new ol.geom.Point(points[1])
            });
            drgPoints.push(drgPoint);
            fldPoints.push(fldPoint);
        }

        let featureLine = new ol.Feature({
            geometry: new ol.geom.LineString(points)
        });

        let vectorLine = new ol.source.Vector({});
        vectorLine.addFeature(featureLine);

        let vectorLineLayer = new ol.layer.Vector({
            source: vectorLine,
            style: new ol.style.Style({
                fill: new ol.style.Fill({ color: '#000', weight: 4 }),
                stroke: new ol.style.Stroke({ color: '#000', width: 2 })
            })
        });
        map.addLayer(vectorLineLayer);
    });

    let drgVectorSource = new ol.source.Vector({
        features: drgPoints
    });
    let fldVectorSource = new ol.source.Vector({
        features: fldPoints
    });

    let drgStyle = new ol.style.Style({
        image: new ol.style.Icon( ({
            anchor: [0.5, 46],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            src: 'drg.svg'
        }))
    });
    let fldStyle = new ol.style.Style({
        image: new ol.style.Icon( ({
            anchor: [0.5, 46],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            src: 'fld.svg'
        }))
    });

    let drgVectorLayer = new ol.layer.Vector({
        source: drgVectorSource,
        style: drgStyle
    });
    let fldVectorLayer = new ol.layer.Vector({
        source: fldVectorSource,
        style: fldStyle
    });

    map.addLayer(drgVectorLayer);
    map.addLayer(fldVectorLayer);
}
