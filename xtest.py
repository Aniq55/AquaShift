import pandas as pd
# from osgeo import gdal
import numpy as np

# User Inputs
FILTER = 3
TL_x = 86
TL_y = 26
BR_x = 89
BR_y = 21


cell_size = 0.0416666666667

# validate -58<x<85 and -180<y<-180
j_start = int((TL_x+180.0)/cell_size)
j_end = int((BR_x+180.0)/cell_size)
i_start = int((-TL_y+85.0)/cell_size)
i_end = int((-BR_y+85.0)/cell_size)

print(i_start, i_end, j_start, j_end)


xll = -180.0
yll = -58.015072


def toLat(x):
    return xll + (x+0.5)*cell_size

def toLon(y):
    return 143 + yll - (y+0.5)*cell_size

xll2 = -180.0
yll2 = -57.999999999974

def toLat2(x):
    return xll2 + (x+0.5)*cell_size

def toLon2(y):
    return 143 + yll2 - (y+0.5)*cell_size

df = pd.read_table("gdfld/gdfld.asc")
df = df.drop(df.index[0:5]) # dropping the data details

valid_datapoints=[]
flood_val=[]

for i, row in enumerate(df.values):
    # print(i)
    if i> i_start and i< i_end:
        x= (str(row)[2:-3]).split()
        for j in range(j_start, j_end+1):
            if x[j]!='-9999':
                if int(x[j])>FILTER:
                    valid_datapoints.append((i,j))
                    flood_val.append(int(x[j]))

# print(len(valid_datapoints))
# print(len(flood_val))

valid_pos=[]

for p in valid_datapoints:
    valid_pos.append((toLon(p[0]), toLat(p[1])))



df2 = pd.read_table("gddrg/gddrg.asc")
df2 = df2.drop(df2.index[0:5]) # dropping the data details


valid_datapoints2=[]
drought_val=[]

for i, row in enumerate(df2.values):
    if i> i_start and i< i_end:
        x= (str(row)[2:-3]).split()
        for j in range(j_start, j_end+1):
            if x[j]!='-9999':
                if int(x[j])>FILTER:
                    valid_datapoints2.append((i,j))
                    drought_val.append(int(x[j]))

# print(len(valid_datapoints))
# print(len(flood_val))

valid_pos2=[]

for p in valid_datapoints2:
    valid_pos2.append((toLon2(p[0]), toLat2(p[1])))

import matplotlib.pyplot as plt


print(len(flood_val))
print(len(drought_val))

j=0
for x in valid_pos:
    plt.scatter(x[1],x[0], color='b',s= flood_val[j], alpha=0.5)
    j=j+1

j=0
for x in valid_pos2:
    plt.scatter(x[1],x[0], color='r', s= drought_val[j], alpha=0.5)
    j=j+1

plt.show()
plt.savefig('fld_drg.png')

# print(valid_pos)





# print(df)
